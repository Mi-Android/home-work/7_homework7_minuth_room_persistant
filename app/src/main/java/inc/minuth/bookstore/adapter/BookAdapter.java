package inc.minuth.bookstore.adapter;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import inc.minuth.bookstore.R;
import inc.minuth.bookstore.callback.MainFragmentCallBack;
import inc.minuth.bookstore.model.Book;
import inc.minuth.bookstore.model.BookWithCategoryName;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.ViewHolder> {

    private List<BookWithCategoryName>bookList;
    private LayoutInflater inflater;
    private MainFragmentCallBack callBack;
    public BookAdapter(List<BookWithCategoryName> bookList, Fragment context) {
        this.bookList=bookList;
        inflater=LayoutInflater.from(context.getActivity());
        callBack=(MainFragmentCallBack)context;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view=inflater.inflate(R.layout.layout_list_data,null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        BookWithCategoryName bookWithCategoryName=getItem(i);
        Book book=bookWithCategoryName.book;
        if(!book.getImage().equals(""))
        {
            viewHolder.imageView.setImageURI(Uri.parse(book.getImage()));
        }
        else
        {
            viewHolder.imageView.setImageResource(R.mipmap.ic_launcher);
        }
        viewHolder.tvTitle.setText(book.getTitle());
        viewHolder.tvSize.setText("Size :"+book.getSize());
        viewHolder.tvPrice.setText("Price :"+book.getPrice()+"$");
        viewHolder.tvCategory.setText(bookWithCategoryName.categoryName);
    }

    @Override
    public int getItemCount() {
        return bookList.size();
    }
    private BookWithCategoryName getItem(int i)
    {
        return bookList.get(i);
    }
    public void search(List<BookWithCategoryName>bookList)
    {
        this.bookList=bookList;
        notifyDataSetChanged();
    }
    class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvTitle;
        TextView tvCategory;
        TextView tvPrice;
        TextView tvSize;
        ImageView imageView;
        ImageButton btnMenu;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvCategory=itemView.findViewById(R.id.tvCategory);
            tvPrice=itemView.findViewById(R.id.tvPrice);
            tvSize=itemView.findViewById(R.id.tvSize);
            tvTitle=itemView.findViewById(R.id.tvTitle);
            imageView=itemView.findViewById(R.id.imgView);
            btnMenu=itemView.findViewById(R.id.btnMenu);
            btnMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu popupMenu=new PopupMenu(inflater.getContext(),v);
                    popupMenu.getMenuInflater().inflate(R.menu.menu_main_fragment,popupMenu.getMenu());
                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId())
                            {
                                case R.id.menu_edit:
                                    callBack.showEditDialog(bookList.get(getAdapterPosition()),getAdapterPosition());
                                    break;
                                case R.id.menu_remove:
                                    callBack.removeBook(bookList.get(getAdapterPosition()));
                                    break;
                                case R.id.menu_read:
                                    callBack.viewDetail(bookList.get(getAdapterPosition()));
                                    break;

                            }
                            return true;
                        }
                    });
                    popupMenu.show();
                }
            });
        }
    }
}
