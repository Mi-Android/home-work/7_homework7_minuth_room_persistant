package inc.minuth.bookstore.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

@Entity(
        foreignKeys = @ForeignKey(
                entity = Category.class,
                parentColumns = "id",
                childColumns = "cateId",
                onDelete = ForeignKey.NO_ACTION
        )
)
public class Book
{
    @PrimaryKey(autoGenerate = true)
    private int id;
    private int cateId;
    private String title;
    private String size;
    private String image;
    private double price;

    public Book() {
    }

    public Book(int cateId, String title, String size, String image, double price) {
        this.cateId = cateId;
        this.title = title;
        this.size = size;
        this.image = image;
        this.price = price;
    }

    public Book(int id, int cateId, String title, String size, String image, double price) {
        this.id = id;
        this.cateId = cateId;
        this.title = title;
        this.size = size;
        this.image = image;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCateId() {
        return cateId;
    }

    public void setCateId(int cateId) {
        this.cateId = cateId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return title;
    }
}
