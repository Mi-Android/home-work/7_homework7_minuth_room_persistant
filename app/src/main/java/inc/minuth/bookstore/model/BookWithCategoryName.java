package inc.minuth.bookstore.model;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Relation;

public class BookWithCategoryName
{
    @Embedded
    public Book book;
    public String categoryName;

    public BookWithCategoryName(Book book, String categoryName) {
        this.book = book;
        this.categoryName = categoryName;
    }
}
