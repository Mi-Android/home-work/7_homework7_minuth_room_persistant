package inc.minuth.bookstore;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.system.Os;
import android.view.View;
import android.webkit.PermissionRequest;
import android.widget.Button;
import android.widget.Toast;

import java.security.Permission;
import java.security.Permissions;

import inc.minuth.bookstore.dao.BookDao;
import inc.minuth.bookstore.dao.CategoryDao;
import inc.minuth.bookstore.database.AppDatabase;
import inc.minuth.bookstore.dialog.AddBookDialog;
import inc.minuth.bookstore.fragment.FragmentMain;
import inc.minuth.bookstore.model.Category;

public class MainActivity extends AppCompatActivity {

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    final int REQUEST_READ_EXTERNAL_PERMISSION=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkPermission();
        fragmentManager=getSupportFragmentManager();
        replaceFragment(new FragmentMain());
    }
    private void replaceFragment(Fragment fragment)
    {
        fragmentTransaction=fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer,fragment);
        if(!(fragment instanceof FragmentMain))
        {
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.commit();
    }
    private void checkPermission()
    {
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M)
        {
            if(!permissionIsGrant())
            {
                requestPermission();
            }
        }
    }
    private boolean permissionIsGrant()
    {
        if(ActivityCompat.checkSelfPermission(this,Manifest.permission.READ_EXTERNAL_STORAGE)==PackageManager.PERMISSION_GRANTED)
        {
            return true;
        }
        return false;
    }
    private void requestPermission()
    {
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},REQUEST_READ_EXTERNAL_PERMISSION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode)
        {
            case REQUEST_READ_EXTERNAL_PERMISSION:
                if(!(permissions.length>0&&grantResults[0]==PackageManager.PERMISSION_GRANTED))
                {
                    Toast.makeText(this,"Permission is denied",Toast.LENGTH_LONG).show();
                }
        }
    }
}
