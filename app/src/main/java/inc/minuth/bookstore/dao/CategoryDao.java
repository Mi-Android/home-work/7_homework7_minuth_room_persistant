package inc.minuth.bookstore.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import inc.minuth.bookstore.model.Category;

@Dao
public interface CategoryDao
{
    @Insert
    void insert(Category... categories);
    @Update
    void update(Category... categories);
    @Delete
    void delete(Category... categories);
    @Query("select * from category")
    List<Category> getAll();
    @Query("select * from category where id = :id")
    Category getOne(int id);
}
