package inc.minuth.bookstore.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import inc.minuth.bookstore.model.Book;
import inc.minuth.bookstore.model.BookWithCategoryName;

@Dao
public interface BookDao
{
    @Insert
    void insert(Book... books);
    @Update
    void update(Book... books);
    @Delete
    void delete(Book... books);
    @Query("select b.id,b.cateId,b.image,b.size,b.title,b.price,c.name as categoryName from book b inner join category c on b.cateId=c.id order by b.id desc")
    List<BookWithCategoryName>getAll();
    @Query("select * from book where id = :id")
    Book getOne(int id);
}
