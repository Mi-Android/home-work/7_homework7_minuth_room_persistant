package inc.minuth.bookstore.callback;

import inc.minuth.bookstore.model.Book;
import inc.minuth.bookstore.model.BookWithCategoryName;

public interface MainFragmentCallBack
{
    void addNewBook(BookWithCategoryName book);
    void showEditDialog(BookWithCategoryName book,int pos);
    void updateBook(BookWithCategoryName book,int pos);
    void removeBook(BookWithCategoryName book);
    void viewDetail(BookWithCategoryName book);
}
