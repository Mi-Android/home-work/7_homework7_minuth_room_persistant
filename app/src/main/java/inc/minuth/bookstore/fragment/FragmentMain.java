package inc.minuth.bookstore.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import inc.minuth.bookstore.R;
import inc.minuth.bookstore.adapter.BookAdapter;
import inc.minuth.bookstore.callback.MainFragmentCallBack;
import inc.minuth.bookstore.dao.BookDao;
import inc.minuth.bookstore.database.AppDatabase;
import inc.minuth.bookstore.dialog.AddBookDialog;
import inc.minuth.bookstore.dialog.UpdateBookDialog;
import inc.minuth.bookstore.dialog.ViewDetailDialog;
import inc.minuth.bookstore.model.BookWithCategoryName;

public class FragmentMain extends Fragment implements MainFragmentCallBack {
    private BookAdapter adapter;
    private List<BookWithCategoryName> books;
    private BookDao bookDao;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_main,container,false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        RecyclerView recyclerView=view.findViewById(R.id.recycler_view);
        GridLayoutManager manager=new GridLayoutManager(getActivity(),2);
        recyclerView.setLayoutManager(manager);
        AppDatabase database=AppDatabase.getINSTANCE(getActivity());
        bookDao=database.bookDao();
        books = bookDao.getAll();
        adapter=new BookAdapter(books,getActivity().getSupportFragmentManager().findFragmentById(R.id.fragmentContainer));
        recyclerView.setAdapter(adapter);
        Button btnDonate=view.findViewById(R.id.btnDonate);
        btnDonate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddBookDialog dialog=new AddBookDialog();
                dialog.show(getActivity().getSupportFragmentManager(),"ADD");
            }
        });
        EditText edtSearch=view.findViewById(R.id.edtSearch);
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                List<BookWithCategoryName>tmpBooks=new ArrayList<>();
                for(BookWithCategoryName bookData:books)
                {
                    if(bookData.book.getTitle().toLowerCase().contains(s.toString().toLowerCase()))
                    {
                        tmpBooks.add(bookData);
                    }
                }
                adapter.search(tmpBooks);
            }
        });

    }


    @Override
    public void addNewBook(BookWithCategoryName book) {
        books.clear();
        books.addAll(bookDao.getAll());
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showEditDialog(BookWithCategoryName book, int pos) {
        UpdateBookDialog dialog=new UpdateBookDialog();
        dialog.setData(book,pos);
        dialog.show(getActivity().getSupportFragmentManager(),"UPDATE");
    }

    @Override
    public void updateBook(BookWithCategoryName book, int pos) {
        books.set(pos,book);
        adapter.notifyItemChanged(pos);
    }

    @Override
    public void removeBook(final BookWithCategoryName book) {
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        builder.setTitle("Delete");
        builder.setMessage("Are you sure to delete?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                books.remove(book);
                adapter.notifyDataSetChanged();
                bookDao.delete(book.book);
            }
        });
        builder.setNegativeButton("No",null);
        AlertDialog dialog=builder.create();
        dialog.show();

    }

    @Override
    public void viewDetail(BookWithCategoryName book) {
        ViewDetailDialog dialog=new ViewDetailDialog();
        dialog.setData(book);
        dialog.show(getFragmentManager(),"Detail");
    }
}
