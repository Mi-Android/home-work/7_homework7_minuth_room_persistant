package inc.minuth.bookstore.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;

import inc.minuth.bookstore.R;
import inc.minuth.bookstore.callback.MainFragmentCallBack;
import inc.minuth.bookstore.dao.BookDao;
import inc.minuth.bookstore.dao.CategoryDao;
import inc.minuth.bookstore.database.AppDatabase;
import inc.minuth.bookstore.model.Book;
import inc.minuth.bookstore.model.BookWithCategoryName;
import inc.minuth.bookstore.model.Category;

public class AddBookDialog extends DialogFragment
{
    private int cateId;
    private int cateIndex=0;
    private Uri imgUri;
    private ViewHolder viewHolder;
    private List<Category> categoryList;
    private static  final int REQUEST_PICK=1;
    private MainFragmentCallBack callBack;
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        callBack=(MainFragmentCallBack)getActivity().getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        View view=getActivity().getLayoutInflater().inflate(R.layout.dialog_add_book,null);
        AppDatabase database=AppDatabase.getINSTANCE(getActivity());
        final BookDao bookDao=database.bookDao();
        CategoryDao categoryDao=database.categoryDao();
        categoryList=categoryDao.getAll();
        if(categoryList.size()==0)
        {
            categoryDao.insert(new Category("Drama"),new Category("Romantic"),new Category("Horror"),new Category("Sweet"));
            categoryList=categoryDao.getAll();
        }
        cateId=categoryList.get(0).getId();
        viewHolder=new ViewHolder(view);
        ArrayAdapter<Category> adapter=new ArrayAdapter<>(getActivity(),android.R.layout.simple_spinner_dropdown_item,categoryList);
        viewHolder.spCategory.setAdapter(adapter);
        viewHolder.spCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cateId=categoryList.get(position).getId();
                cateIndex=position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        viewHolder.btnBrowse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent,REQUEST_PICK);
            }
        });
        builder.setTitle("Add New Book");
        builder.setView(view);
        builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String imgPath=imgUri!=null?imgUri.toString():"";
                Book book=new Book(cateId,viewHolder.edtTile.getText().toString(),viewHolder.edtSize.getText().toString(),imgPath,Double.parseDouble(viewHolder.edtPrice.getText().toString()));
                bookDao.insert(book);
                callBack.addNewBook(new BookWithCategoryName(book,categoryList.get(cateIndex).getName()));

            }
        });
        builder.setNegativeButton("Cancel",null);
        return builder.create();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode)
        {
            case REQUEST_PICK:
                if(resultCode==Activity.RESULT_OK)
                {
                    imgUri=data.getData();
                    viewHolder.imageView.setImageURI(imgUri);
                }
        }
    }

    class ViewHolder
    {
        EditText edtTile;
        EditText edtSize;
        EditText edtPrice;
        Spinner spCategory;
        ImageView imageView;
        Button btnBrowse;
        public ViewHolder(View v)
        {
            edtSize=v.findViewById(R.id.edtSize);
            edtTile=v.findViewById(R.id.edtTitle);
            edtPrice=v.findViewById(R.id.edtPrice);
            spCategory=v.findViewById(R.id.spCategory);
            imageView=v.findViewById(R.id.imgView);
            btnBrowse=v.findViewById(R.id.btnBrowse);
        }
    }
}
