package inc.minuth.bookstore.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import inc.minuth.bookstore.R;
import inc.minuth.bookstore.model.Book;
import inc.minuth.bookstore.model.BookWithCategoryName;

public class ViewDetailDialog extends DialogFragment
{
    private BookWithCategoryName data;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder=new AlertDialog.Builder(getContext());
        View view=getActivity().getLayoutInflater().inflate(R.layout.dialog_view_detail,null);
        ViewHolder viewHolder=new ViewHolder(view);
        Book book=data.book;
        if(!book.getImage().equals(""))
        {
            viewHolder.imageView.setImageURI(Uri.parse(book.getImage()));
        }
        else
        {
            viewHolder.imageView.setImageResource(R.mipmap.ic_launcher);
        }
        viewHolder.tvTitle.setText(book.getTitle());
        viewHolder.tvSize.setText("Size :"+book.getSize());
        viewHolder.tvPrice.setText("Price :"+book.getPrice()+"$");
        viewHolder.tvCategory.setText(data.categoryName);

        builder.setView(view);
        builder.setPositiveButton("Ok",null);
        return builder.create();
    }
    public void setData(BookWithCategoryName data)
    {
        this.data=data;
    }
    class ViewHolder
    {
        TextView tvTitle;
        TextView tvCategory;
        TextView tvPrice;
        TextView tvSize;
        ImageView imageView;
        public ViewHolder(View itemView) {
            tvCategory=itemView.findViewById(R.id.tvCategory);
            tvPrice=itemView.findViewById(R.id.tvPrice);
            tvSize=itemView.findViewById(R.id.tvSize);
            tvTitle=itemView.findViewById(R.id.tvTitle);
            imageView=itemView.findViewById(R.id.imgView);
        }
    }
}
