package inc.minuth.bookstore.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import inc.minuth.bookstore.dao.BookDao;
import inc.minuth.bookstore.dao.CategoryDao;
import inc.minuth.bookstore.model.Book;
import inc.minuth.bookstore.model.Category;

@Database(
        version = 1,
        entities = {
                Book.class,
                Category.class
        }
)
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase INSTANCE;
    public abstract BookDao bookDao();
    public abstract CategoryDao categoryDao();
    public static AppDatabase getINSTANCE(Context context)
    {
        if(INSTANCE==null)
        {
            INSTANCE=Room.databaseBuilder(context,AppDatabase.class,"book_management_db")
            .allowMainThreadQueries().build();
        }
        return INSTANCE;
    }
}
